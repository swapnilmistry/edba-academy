import React from "react";

// function Component() {
//     return <div>
//         <p>This is a p tag</p>
//         <strong>This is a strong tag</strong>
//     </div>
// }

function AnotherComponent() {
    return <div>
        <p>Another Component</p>
    </div>
}

function SecondaryComponent() {
    return <div>
        <p>Secondary Component</p>
    </div>
}

class Component extends React.Component {
    render() {
    return <div>
        <p>Class Component</p>
        <p>This is a p tag</p>
        <strong>This is a strong tag</strong>
    </div>
    }
}

// export default Component
export {
    Component,
    AnotherComponent,
    SecondaryComponent
}
import React, {useEffect, useState} from "react";

function ParentComponentLifeCycle() {
    const [show, setShow] = useState(false)

    const onClick = () => {
        setShow((prev) => !prev)
    }
    return <div>
        <button onClick={onClick}>{show ? "Hide Component" : "Show Component"}</button>
        {show ? <ComponentLifeCycle /> : null}
    </div> 
}

// function ComponentLifeCycle() {
//     const [count, setCount] = useState(0)
//     const [input, setInput] = useState("")

//     useEffect(() => {
//         /**
//          * This will run on mount
//          * This will also run whenever any state changes occurs
//          */
//         console.log("UseEffect called without dependency array.")

//         return () => {
//             /**
//              * This get's called when comonent's get unmounted
//              * Can be used for cleanup tasks
//              */
//             console.log("Component unmounted...")
//         }
//     })

//     useEffect(() => {
//         /**
//          * This will run on mount
//          * This will not run whenever any state changes occurs
//          * This will run only once throughout lifecycle of component
//          */
//         console.log("UseEffect called with empty dependency array.")
//     }, [])

//     useEffect(() => {
//         /**
//          * This will run on mount
//          * This will not run whenever any state changes occurs for count
//          */
//         console.log("UseEffect called with count in dependency array.")
//     }, [count])

//     useEffect(() => {
//         /**
//          * This will run on mount
//          * This will not run whenever any state changes occurs for input
//          */
//         console.log("UseEffect called with input in dependency array.")
//     }, [input])


//     const onClick = () => {
//         setCount((prevCount) => prevCount + 1)
//     }

//     const onChange = (event) => {
//         setInput(event.target.value)
//     }

//     return <>
//     Component Life ComponentLifeCycle
//     <h1>{count}</h1>
//     <input value={input} onChange={onChange}/>
//     <button onClick={onClick}>Incr</button>
//     </>
// }


class ComponentLifeCycle extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            count: 0,
            input: ""
        }
    }

    componentDidMount() {
        /**
         * This will run on mount
         * This will not run whenever any state changes occurs for count
         */
        console.log("Class component mounted...")
    }

    componentWillUnmount() {
        /**
         * This will get called when component unmounts
         */
        console.log("Class component unmounted...")
    }

    shouldComponentUpdate(nextProps, nextState) {
        // console.log(nextProps, nextState, this.state)
        /**
         * This will run evertime whenver state changes occurs
         * Returning true will re-render the component
         * Returning false will not re-render the component
         */
        // if (nextState.count !== this.state.count) {
        //     return true
        // }
        return true
    }

    onClick = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    onChange = (event) => {
        this.setState({
            input: event.target.value
        })
    }

    render() {
    return <>
    ComponentLifeCycle
    <h1>{this.state.count}</h1>
    <input value={this.state.input} onChange={this.onChange}/>
    <button onClick={this.onClick}>Incr</button>
    </>
    }
}

export default ParentComponentLifeCycle
import React from "react";

function ComponentB(props) {
    console.log("props: ", props)
    return <div>
    <h2>In ComponentB {props.heading}</h2>
    <p>{JSON.stringify(props.details)}</p>
</div> 
}

export default ComponentB
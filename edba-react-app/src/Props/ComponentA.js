import React from "react";
import ComponentB from "./ComponentB";

function ComponentA() {

    const details = {
        fname: "John",
        lastName: "Doe"
    }

    return <div>
        <h2>In ComponentA</h2>
        <ComponentB details={details} heading={"Heading"}/>
    </div>
}

export default ComponentA
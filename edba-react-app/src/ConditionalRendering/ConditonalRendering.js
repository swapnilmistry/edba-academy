import React from "react";


function FirstComponent() {
    return <h1>First component rendered</h1>
}

function SecondaryComponent() {
    return <h1>Second component rendered</h1>
}


function ConditionalRendering() {
    const showFirstComponent = true

    return showFirstComponent ? <FirstComponent /> : <SecondaryComponent />
}

export default ConditionalRendering
import React from "react";
import styles from "./StylingComponent.module.css"

function StylingComponent() {
    return <div style={{
        display: "flex",
        width: "100vw",
        height: "100vh",
        justifyContent: "center",
        flexDirection: "column",
        alignItems: "center"
    }}>
        <h1>Styling Component</h1>
        <div className={styles["square-container"]}></div>
        <div id={styles["square-container"]}></div>
    </div>
}

export default StylingComponent
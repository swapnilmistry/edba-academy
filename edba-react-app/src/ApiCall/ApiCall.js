// https://reqres.in/api/users?page=1
// https://reqres.in/api/users?page=1&per_page=3

import React, {useState, useEffect} from "react";

const ENDPOINT = "https://reqres.in/api/users"
function ApiCall() {
    const [userData, setUserData] = useState()

    useEffect(() => {
        fetch(ENDPOINT)
        .then((response) => {
            if (response.status === 200) {
                return response.json()
            }
            throw Error("Something went wrong...")
        })
        .then((data) => {
            setUserData(data)
        }).catch((_error) => {})
    }, [])

    // async function getDataFromServer() {
    //     try {
    //         const response = await fetch(ENDPOINT).then(() => response.json())
    //         console.log(response)
    //     } catch(error) {}
    // }

    // useEffect(() => {
    //     getDataFromServer()
    // })

    if (!userData) {
        return null
    }

    return <div>
        {
            userData.data.map(element => {
                return <h1>{element.email}</h1>
            })
        }
    </div>
}

export default ApiCall
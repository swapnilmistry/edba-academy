// import ApiCall from './ApiCall/ApiCall';
import './App.css';
import TodoContainer from './TodoList';
// import ParentComponentLifeCycle from './ComponentLifecycle/ComponentLifeCycle';
// import StateHook from './Hooks/StateHook';
// import ClassStateManagement from './ClassStateManagement/ClassStateManagement';
// import ConditionalRendering from './ConditionalRendering/ConditonalRendering';
// import ComponentA from './Props/ComponentA';
// import Jsx from "./Jsx/Jsx"
// import Component from './Components/Components';
// import {AnotherComponent, SecondaryComponent, Component} from "./Components/Components"
// import StylingComponent from './Styling/StylingComponent';
function App() {
  return <div>
    {/* <Jsx /> */}
    {/* <Component />
    <AnotherComponent />
    <SecondaryComponent /> */}
    {/* <StylingComponent /> */}
    {/* <ComponentA /> */}
    {/* <ConditionalRendering /> */}
    {/* <ClassStateManagement /> */}
    {/* <StateHook /> */}
    {/* <ParentComponentLifeCycle /> */}
    {/* <ApiCall /> */}
    <TodoContainer />
  </div>
}

export default App;

import React, {useState} from "react";


function StateHook() {
    const [count, setCount] = useState(0)

    function incr() {
        setCount((prevState) => {
            return prevState + 1
        })
        // setCount((prevState) => {
        //     return prevState + 1
        // })
    }

    function decr() {
        setCount((prevState) => {
            return prevState - 1
        })
    }

    return <div>
    <button onClick={incr}>Incr Func</button>
    <p>{count}</p>
    <button onClick={decr}>Derc Func</button>
</div>
}

export default StateHook
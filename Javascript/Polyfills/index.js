/**
 * A polyfill is a piece of code (usually JavaScript on the Web) used to provide modern functionality on older browsers that do not natively support it.
 */

let array = [12, 22, 30, 412, 50]

Array.prototype.customForEach = function (fnArgs) {
        for (let i = 0; i < this.length; i++) {
            fnArgs(this[i], i, this)
        }
}

// Polyfill for forEach
array.customForEach((element, index, arr) => {
    console.log(element, index, arr)
})

// Polyfill for map
Array.prototype.customMap = function(fnArgs) {
    let array = []
    for (let i = 0; i < this.length; i++) {
        array.push(fnArgs(this[i], i, this))    
    }
    return array
}

const customMapResult = array.customMap((element) => {
    return element / 10
})

console.log(customMapResult)

//Polyfill for filter
Array.prototype.customFilter = function(fnArgs) {
    let array = []
    for (let i = 0; i < this.length; i++) {
        if (fnArgs(this[i], i, this)) {
            array.push(this[i])
        }
    }
    return array
}

const customFilterResult = array.customFilter((element) => {
    if (element > 30) {
        return true
    }
    return false
})

console.log(customFilterResult)

// Polyfill for reduce
Array.prototype.customReduce = function(fnArgs, initialValue) {
    let accumlator = initialValue ? initialValue : undefined

    for (let i = 0; i < this.length; i++) {
       if (!accumlator) {
        accumlator = this[i]
       } else {
        accumlator = accumlator + fnArgs(accumlator, this[i], i, this)
       } 
    }
    return accumlator
}

const customReduceResult = array.customReduce((accumlator, element, index, arr) => {
    return accumlator + element
}, 1000)

console.log(customReduceResult)

// Polyfill for every
Array.prototype.customEvery = function(fnArgs) {
    let count = 0
    for (let i = 0; i < this.length; i++) {
        if (fnArgs(this[i], i, this)) {
            count = count + 1
        }
    }

    return count === this.length
}

const customEveryResult = array.customEvery((element, index, arr) => {
    if (element > 10 && element < 500) {
        return true
    }
    return false
})

console.log(customEveryResult)

// Polyfill for some
Array.prototype.customSome = function(fnArgs) {
    for (let i = 0; i < this.length; i++) {
        if (fnArgs(this[i], i, this)) {
            return true
        }
    }
    return false
}

const customSomeResult = array.customSome((element, index, arr) => {
    if (element === 1) {
        return true
    }
    return false
})

console.log(customSomeResult)
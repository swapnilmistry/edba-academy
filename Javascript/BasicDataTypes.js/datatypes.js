
let str = "This is a JS session."
str = str + 'This is a appended string'

function jsFn(name, sessionFrom) {
    console.log("Name is " + name + " " + sessionFrom)
    console.log(`String Literal: Name is ${name} ${sessionFrom}`)
}

jsFn("John", "edba")

let number = 2.45

console.log(str)
console.log(number)


const fn = () => {}
function fn1() {}

console.log(typeof ``) // "" or '' type => strig
console.log(typeof 1.0234) // decimal, float, int type => number
console.log(typeof {}) // [], null, type => object
console.log(typeof fn1) // Arrow function, traditional function, type => function

console.log(Array.isArray([])) // Helps to check if datatype is array or not


function compare(arg1, arg2) {
    if (arg1 == arg2) {
        console.log("Both are similar..")
        return
    }
    console.log("Not similar...")
}

compare(10, "10")

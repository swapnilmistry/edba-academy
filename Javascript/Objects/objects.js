let myObj = {
    fName: "John",
    lName: "Doe",
    l_name: "Susan",
    "l-name": "Test"
}

let anotherObj = Object.create({})

myObj["custom-property"] = 12

// console.log("My object: ", myObj)

anotherObj["number"] = 10.2

delete myObj["custom-property"]


// console.log("My object: ", myObj)
// console.log("Another object: ", anotherObj)
/**
 * Protype can be used to create or inherit exixting properties or objects in JS
 * Example:
 *  - Date will be inherited from Date.prototype
 *  - Array will be inherited from Array.prototype
 * The Object.prototype is the prototype which is present at the top
 * __proto__ is only available when you create a object
 * prototype is available only with functions (having constructor & properties) and when we use them using new keyword
 */

const person = {
    name: "John",
    age: 10,
    gender: "M"
}

const secondaryObj = {
    testField: "value"
}

console.log(person.__proto__) // constructor
console.log(person.prototype) // undefined

const teacher = {
    subject: "Javascript"
}

teacher.__proto__ = person
console.log(teacher)
teacher.__proto__ = secondaryObj
console.log(teacher)

console.log(teacher.__proto__ === Object.prototype)
console.log(teacher.__proto__ === person)

function Person(fname, lname, age) {
    this.fname = fname;
    this.lname = lname;
    this.age = age
}

Person.prototype.nationality = "Indian"
Person.prototype.name = function() {
    return `${this.fname} ${this.lname}`
}

const newPerson = new Person("John", "Doe", 24)
console.log(newPerson.name())


// var name = "Jane"

// const obj = {
//     name: "John",
//     fn: () => {
//         console.log(this.name)
//     }
// }

// obj.fn()

/**
 * Promises can be used for achieving async task.
 * Promises has 3 states:   
 * 1. Pending => promises is under execution
 * 2. Fullfilled => promises is maked resolved
 * 3. Rejected => promises is marked rejected
 */
function executePromise() {
 const myPromise = new Promise((resolve, _reject) => {
    setTimeout(() => {
        let add = 1 + 12
        resolve(add)
    }, 3000)
 })

//  console.log(myPromise)
 myPromise.then((data) => {
    console.log("Success: ", data)
    throw Error(data * 5)
 }).then((response) => {
    console.log("Response from above then block: ", response)
 }).catch((error) => {
    console.log("Error: ", error)
    return "Returning error from catch..."
 }).then((result) => {
    console.log("Then after catch: ", result)
 })
}

// executePromise()

function getDataFromServer() {
    fetch("https://reqres.in/api/users").then((response) => {
    if (response.status === 200) {
        return response.json()
    }
    throw Error("An error occured while fetching data...")
}).then((data) => {
    console.log(data)
}).catch((error) => {
    console.log(error)
})
}

// getDataFromServer()

/**
 * Promise.all() => Array of promises
 * Waits until all promises are resolved
 * Fails when any one of the the promise is rejected
 * To overcome this add catch block with every promise call
 * Even when a single promise is rejected promise.all with get resolved status
 * It will return undefined in response for the rejected promises
 */

const fetchUser = fetch("https://reqres.in/api/users")
.then((response) => response.json())
.then((data) => {
    return new Promise((resolve, _reject) => {
        setTimeout(() => {
            resolve(data)
        }, 5000)
    })
}).catch((error) => {
    console.log("Error getting users data..")
})

const fetchRandomData = fetch("https://random-data-api.com/api/stripe/random_stripe")
.then((response) => response.json())
.then((data) => {
    throw Error("Error getting random data...")
}).catch((error) => {
    console.log("Error getting random data..")
})

Promise.all([fetchUser,fetchRandomData])
.then((data) => {
    // [userData, randomData]
    console.log("Promise.all response: ", data)
}).catch((error) => {
    console.log("Any api call got rejected")
})


/**
 * Promise.any() => Array of promises
 * Dosen't wait for all promises to get resolved
 * It will return response of the first resolved promise
 */

const fetchUserAny = fetch("https://reqres.in/api/users")
.then((response) => response.json())
.then((data) => {
    return new Promise((resolve, _reject) => {
        setTimeout(() => {
            resolve(data)
        }, 5000)
    })
}).catch((error) => {
    console.log("Error getting users data..")
})

const fetchRandomDataAny = fetch("https://random-data-api.com/api/stripe/random_stripe")
.then((response) => response.json())
.then((data) => data).catch((error) => {
    console.log("Error getting random data..")
})

Promise.any([fetchUserAny, fetchRandomDataAny]).then((response) => {
    console.log("Promise.any response: ", response)
}).catch((_error) => {})

/**
 * Promise.race() => Array of promises
 * It will retrun response of the first executed promise
 * It will not wait for other promises which are being executed and they are independent of each other
 */

const fetchUserRace = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve({
            data: "User data"
        })
    }, 5000)
})

const fetchRandomDataRace = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve({
            data: "Random data"
        })
    }, 5000)
})

Promise.race([fetchUserRace, fetchRandomDataRace]).then((response) => {
    console.log("Promise.race response: ", response)
}).catch((_error) => {})

// ==========================================
// function job() {
//     return new Promise(function(resolve, reject) {
//         reject();
//     });
// }
// let promise = job();
// promise.then(function() {
//     console.log('Success 1');
// }).then(function() {
//     console.log('Success 2');
// }).then(function() {
//     console.log('Success 3');
// }).catch(function() {
//     console.log('Error 1');
// }).then(function() {
//     console.log('Success 4');
// });

// Ans => Error1, Success 4

// ==========================================
// function job(state) {
//     return new Promise(function(resolve, reject) {
//         if (state) {
//             resolve('success');
//         } else {
//             reject('error');
//         }
//     });
// }
// let promise2 = job(true);
// promise2.then(function(data) {
//     console.log(data);
//     return job(false);
// }).catch(function(error) {
//     console.log(error);
//     return 'Error caught';
// }).then(function(data) {
//     console.log(data);
//     return job(true);
// }).catch(function(error) {
//     console.log(error);
// });

// Ans => success, error, Error caught

// ==========================================

// function job(state) {
//     return new Promise(function(resolve, reject) {
//         if (state) {
//             resolve('success');
//         } else {
//             reject('error');
//         }
//     });
// }
// let promise3 = job(true);
// promise3.then(function(data) {
//     console.log(data);
//     return job(true);
// }).then(function(data) {
//     if (data !== 'victory') {
//         throw 'Defeat';
//     }
//     return job(true);
// }).then(function(data) {
//     console.log(data);
// }).catch(function(error) {
//     console.log(error);
//     return job(false);
// }).then(function(data) {
//     console.log(data);
//     return job(true);
// }).catch(function(error) {
//     console.log(error);
//     return 'Error caught';
// }).then(function(data) {
//     console.log(data);
//     return new Error('test'); // We are not throwing error we are just returning
// }).then(function(data) {
//     console.log('Success:', data.message);
// })
// .catch(function(data) {
//   console.log('Error:', data.message);
// });

// Ans => success, Defeat, error, Error caught, Success: test
let myArr = [1, 2, 3, 4, 5]

 console.log(`Array: ${myArr}`)

 const newLength = myArr.push("a")

 console.log(`Array after pushing item: ${myArr}`, newLength)

 const deletedItem = myArr.pop()

 console.log(`Array after poping: ${myArr}`, deletedItem)

 for (let i = 0; i < myArr.length; i++) {
     if (i > 3) {
         break
     }
     console.log(myArr[i])
 }


 myArr.forEach((element, index, arr) => {
     console.log(element, index, arr)
 })

 const returnedValue = myArr.map((element, index, arr) => {
      console.log(element, index, arr)
     return element * element
 })

 console.log(returnedValue)

 const filteredArray = myArr.filter((element, index, arr) => {
     if (element > 3) {
         return element
     }
 })

 console.log(filteredArray)


//  function getSum(fn) {
//      let sum = 0

//      for(let i = 0; i < myArr.length; i++) {
//          sum = sum + myArr[i]
//         //   fn(sum, myArr[i], i, myArr)
//      }

//      return sum
//  }

//  const result = getSum()
//  console.log(result)

 const reduceResult = myArr.reduce((accumlator, element, index, arr) => {
     console.log("Accumator: ", accumlator, index)
     return accumlator + element
 })

 console.log(reduceResult)


 const everyResult = myArr.every((element, index, arr) => element > 0)
 console.log(everyResult)

 const someResult = myArr.some((element, index, arr) => element > 4)
 console.log(someResult)